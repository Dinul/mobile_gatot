package com.example.tot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void equipment(View v){
        Intent i = new Intent(this, equipment.class);
        startActivity(i);
    }
    public void fillstat(View v){
        Intent i = new Intent(this, fillstat.class);
        startActivity(i);
    }
    public void petMenu(View v){
        Intent i = new Intent(this, petMenu.class);
        startActivity(i);
    }
}