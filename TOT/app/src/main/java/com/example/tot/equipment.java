package com.example.tot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class equipment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment);
    }
    public void weap(View v){
        Intent i = new Intent(this, weap.class);
        startActivity(i);
    }
    public void armor(View v){
        Intent i = new Intent(this, armor.class);
        startActivity(i);
    }
    public void add(View v){
        Intent i = new Intent(this, add.class);
        startActivity(i);
    }

    public void ring(View v){
        Intent i = new Intent(this, ring.class);
        startActivity(i);
    }

}