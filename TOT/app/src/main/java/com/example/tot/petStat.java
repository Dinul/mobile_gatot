package com.example.tot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class petStat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_stat);
    }
    public void close(){
        View view = this.getCurrentFocus();
        if(view != null){
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
    public void startButton(View view) {
        EditText max = findViewById(R.id.maxtext);
        int maxL = Integer.parseInt(max.getText().toString());
        EditText pot = findViewById(R.id.pottext);
        int potS = Integer.parseInt(pot.getText().toString());
        EditText bonus = findViewById(R.id.bonustext);
        int fuse = Integer.parseInt(bonus.getText().toString());
        TextView lihat = findViewById(R.id.hasil);

        for(int i=1; i<=maxL;i++){
            double stat=Math.floor(i*potS/100)+1+fuse+(i-1);
            if(stat>=250 && stat<=255){
                lihat.setText("Isi Stat Pada Level\n" +i+"\n\nJumlah Stat\n"+(int)stat);
            }
        }

        close();
    }

    public void resetButton(View view){
        EditText max = findViewById(R.id.maxtext);
        EditText pot = findViewById(R.id.pottext);
        EditText bonus = findViewById(R.id.bonustext);
        max.setText("0");
        pot.setText("0");
        bonus.setText("0");
        TextView lihat = findViewById(R.id.hasil);
        lihat.setText("");
    }
}