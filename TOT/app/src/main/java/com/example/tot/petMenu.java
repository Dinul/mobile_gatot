package com.example.tot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class petMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_menu);
    }

    public void petAtribut(View v){
        Intent i = new Intent(this, petAtribut.class);
        startActivity(i);
    }
    public void levelingSkill(View v){
        Intent i = new Intent(this, levelingSkill.class);
        startActivity(i);
    }
    public void petStat(View v){
        Intent i = new Intent(this, petStat.class);
        startActivity(i);
    }

}